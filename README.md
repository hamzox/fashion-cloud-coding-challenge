# fashion-cloud-coding-challenge

## Prerequisites

You will need the following things properly installed on your computer. 

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/) (with npm)

## Installation

* `git clone <repository-url>` this repository
* `npm install`
* `npm run dev`

### Running Tests

* `npm test`
