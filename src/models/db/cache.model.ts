// modules
import mongoose from "mongoose";

// services & helpers
import appHelper from "../../helpers/app.helper";

const Schema = mongoose.Schema;
const CacheSchema = new Schema({
	key: {
		type: String,
		trim: true,
		index: true
	},
	ttl: {
		type: Number,
		default: appHelper.getCacheTtl()
	},
	content: {
		type: String,
		default: appHelper.generateRandomCacheValue() // generates a random String
	},
	modifiedAt: {
		type: Date,
		default: Date.now,
		index: true
	},
	createdAt: {
		type: Date,
		default: Date.now,
		index: true
	}
}, {
	collection: "cache"
});

CacheSchema.pre("save", appHelper.validateCacheRecord);

export default mongoose.model("Cache", CacheSchema);
