// modules
import { Application } from "express";

// controllers
import cacheController from "./controller/cache.controller";

// constants & config
import { RouteConstants } from "./constants/route.constant";

export default class Routes {

    private api = RouteConstants.ROUTE_API;

    constructor(app: Application) {
        // update & create
        app.route(this.api + RouteConstants.CACHE).patch(cacheController.createOrUpdate);

        // get
        app.route(this.api + RouteConstants.CACHE + "/:key").get(cacheController.get);
        app.route(this.api + RouteConstants.CACHE).get(cacheController.getAll);

        // delete
        app.route(this.api + RouteConstants.CACHE + "/:key").delete(cacheController.remove);
        app.route(this.api + RouteConstants.CACHE).delete(cacheController.removeAll);
    }
}
