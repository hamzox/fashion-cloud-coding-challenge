export const SERVER_RESPONSE_CODES = {
    STATUS_OK: 200,
    UNAUTHORIZED: 401,
    INTERNAL_SERVER_ERROR: 500,
    BAD_REQUEST: 400,
    NOT_FOUND: 404
};

export const SERVER_RESPONSE_MESSAGES = {
    // success messages
    SUCCESS: "Success",

    // unsuccessful messages
    UNSUCCESSFULL_OPERATION: "Unsuccessful operation",
    SOMETHING_WENT_WRONG: "Something went wrong!",
    VALIDATION_ERROR: "Validation error",

    // general messages
    CACHE_MISS : "Cache Miss!",
    CACHE_HIT : "Cache Hit!",
};
