export class RouteConstants {
    public static ROUTE_API = "/api/";

    // cache module
    public static CACHE = "cache";
}
